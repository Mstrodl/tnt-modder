def APP_NAME = "Tooth and Tail Mod Manager"
def WINMETA = "--win32metadata.CompanyName=\"heatingdevice\" --win32metadata.FileDescription=\"${APP_NAME}\" --win32metadata.ProductName=\"${APP_NAME}\" --win32metadata.InternalName=\"${APP_NAME}\""
def ign = "'releases|tmpf'"
def buildnum = currentBuild.number

node("master") {
    echo "Creating directory..."
    sh "mkdir -m 777 -p /var/www/html/strodl/tnt-modder/ci/${buildnum}"
    sh "rm -rf /var/www/html/strodl/tnt-modder/ci/latest"
    sh "ln -s /var/www/html/strodl/tnt-modder/ci/${buildnum} /var/www/html/strodl/tnt-modder/ci/latest"
}

parallel(
"linux": {
    node ("master") {
        stage('Cloning Linux') {
            deleteDir()
            git 'https://gitlab.com/Mstrodl/tnt-modder.git'
            if(isUnix()) {
                sh "rm -rf tmpf releases"
                sh "mkdir tmpf releases"
                } else {
                if(fileExists('tmpf')) {
                    bat(/rd tmpf \/q \/s/)
                }
                if(fileExists('releases')) {
                    bat(/rd releases \/q \/s/)
                }
                bat(/mkdir tmpf/)
                bat(/mkdir releases/)
            }
        }
        stage('Installing modules Linux') {
            if (isUnix()) {
                sh "npm install -g electron-packager electron"
                sh "npm install"
                } else {
                bat(/npm install -g electron-packager electron electron-installer-windows/)
                bat(/npm install/)
            }
        }
        stage('Building Linux x64') {
            if(isUnix()) {
                sh "electron-packager . \"${APP_NAME}\" --out=tmpf --platform=linux --arch=x64 --icon=logo --ignore=${ign}"
                } else {
                bat(/electron-packager . "${APP_NAME}" --out=tmpf --platform=linux --arch=x64 --icon=logo --ignore='releases^^^|tmpf'/)
            }
        }
        stage('Building Linux x86') {
            if(isUnix()) {
                sh "electron-packager . \"${APP_NAME}\" --out=tmpf --platform=linux --arch=ia32 --icon=logo --ignore=${ign}"
                } else {
                bat(/electron-packager . "${APP_NAME}" --out=tmpf --platform=linux --arch=ia32 --icon=logo --ignore=${ign}/)
            }
        }
        stage('Compressing Linux builds') {
            if(isUnix()) {
                sh "zip -r \"releases/${APP_NAME}-linux-x64.zip\" \"tmpf/${APP_NAME}-linux-x64\""
                sh "zip -r \"releases/${APP_NAME}-linux-ia32.zip\" \"tmpf/${APP_NAME}-linux-ia32\""
                sh "cp releases/* /var/www/html/strodl/tnt-modder/ci/${buildnum}"
                } else {
                bat(/"C:Program Files (x86)\\7-Zip\\7z.exe" a -r "releases\${APP_NAME}-linux-x64.zip" -w "tmpf\${APP_NAME}-linux-x64" -mem=AES256/)
                bat(/"C:Program Files (x86)\\7-Zip\\7z.exe" a -r "releases\${APP_NAME}-linux-ia32.zip" -w "tmpf\${APP_NAME}-linux-ia32" -mem=AES256/)
                bat(/pscp -i \id_rsa.ppk releases\* server@strodl.tk:\/var\/www\/html\/strodl\/tnt-modder\/ci\/${buildnum}/)
            }
        }
    }
},
"mac": {
    node ("mac") {
        stage('Cloning Mac') {
            deleteDir()
            git 'https://gitlab.com/Mstrodl/tnt-modder.git'
            sh "rm -rf mac-tmpf mac-releases"
            sh "mkdir mac-tmpf mac-releases"
        }
        stage('Installing modules Mac') {
            sh "npm install -g electron-packager electron"
            sh "npm install"
        }
        stage('Building Mac') {
            sh "electron-packager . \"${APP_NAME}\" --out=mac-tmpf --platform=darwin --arch=x64 --icon=logo --protocol-name=\"Tooth And Tail Mod Manager\" --protocol=\"tnt-modder\" --ignore=${ign}"
        }
        stage('Grabbing create-dmg tool Mac') {
            sh "rm -rf create-dmg"
            sh "git clone https://github.com/andreyvit/yoursway-create-dmg.git create-dmg"
        }
        stage('Creating DMG') {
            sh "./create-dmg/create-dmg \
            --volname '${APP_NAME}' \
            --volicon 'logo.icns' \
            --background 'dmg-background.png' \
            --window-pos 200 120 \
            --window-size 778 400 \
            --icon-size 100 \
            --icon '${APP_NAME}.app' 200 190 \
            --hide-extension '${APP_NAME}.app' \
            --app-drop-link 600 185 \
            'mac-releases/${APP_NAME}-darwin-x64.dmg' \
            'mac-tmpf/${APP_NAME}-darwin-x64/'"
            sh "scp \"mac-releases/${APP_NAME}-darwin-x64.dmg\" server@strodl.tk:/var/www/html/strodl/tnt-modder/ci/${buildnum}"
        }
    }
},
"windows": {
    node ("windows") {
        stage('Cloning Windows') {
            deleteDir()
            git 'https://gitlab.com/Mstrodl/tnt-modder.git'
            if(fileExists('win-tmpf')) {
                bat(/rd win-tmpf \/q \/s/)
            }
            if(fileExists('win-releases')) {
                bat(/rd win-releases \/q \/s/)
            }
            bat(/mkdir win-tmpf/)
            bat(/mkdir win-releases/)
        }
        stage('Installing modules Windows') {
            bat(/npm install -g electron-packager electron electron-installer-windows/)
            bat(/npm install/)
        }
        stage('Building Windows x64') {
            bat(/electron-packager . "tnt-modder" --out=win-tmpf --platform=win32 --arch=x64 --icon=logo ${WINMETA} --ignore='releases^^^|tmpf'/)
        }
        stage('Building Windows x86') {
            bat(/electron-packager . "tnt-modder" --out=win-tmpf --platform=win32 --arch=ia32 --icon=logo ${WINMETA} --ignore='releases^^^|tmpf'/)
        }
        stage('Waiting for version number') {
            def userInput = input(
             id: 'userInput', message: 'Enter symver: ', parameters: [
                [$class: 'TextParameterDefinition', defaultValue: '0.1.1', description: 'New Version', name: 'ver'],
                [$class: 'TextParameterDefinition', defaultValue: '0.1.0', description: 'Old Version', name: 'oldver']
            ])
            bat(/copy config.json config32.json/)
            bat(/copy config.json config64.json/)
            bat(/echo "version": "${userInput['ver']}","remoteReleases": "http:\/\/squirrel.strodl.tk\/update\/win32\/${userInput['oldver']}"} >>config32.json/)
            bat(/echo "version": "${userInput['ver']}","remoteReleases": "http:\/\/squirrel.strodl.tk\/update\/win64\/${userInput['oldver']}"} >>config64.json/)
        }
        stage('Creating Squirrel Packages') {
            bat(/electron-installer-windows --src "win-tmpf\tnt-modder-win32-x64" --dest "win-tmpf\${APP_NAME}-win32-x64-SQUIRREL" --config config64.json/)
            bat(/electron-installer-windows --src "win-tmpf\tnt-modder-win32-ia32" --dest "win-tmpf\${APP_NAME}-win32-ia32-SQUIRREL" --config config32.json/)
            // bat(/"C:\\Program Files (x86)\\7-Zip\\7z.exe" a -r "win-releases\${APP_NAME}-win32-ia32-SQUIRREL.zip" -w "win-tmpf\${APP_NAME}-win32-ia32-SQUIRREL" -mem=AES256/)
            // bat(/"C:\\Program Files (x86)\\7-Zip\\7z.exe" a -r "win-releases\${APP_NAME}-win32-x64-SQUIRREL.zip" -w "win-tmpf\${APP_NAME}-win32-x64-SQUIRREL" -mem=AES256/)
            // bat(/"C:\\Program Files (x86)\\7-Zip\\7z.exe" a -r "win-releases\${APP_NAME}-win32-ia32.zip" -w "tmpf\${APP_NAME}-win32-ia32" -mem=AES256/)
            // bat(/"C:\\Program Files (x86)\\7-Zip\\7z.exe" a -r "win-releases\${APP_NAME}-win32-x64.zip" -w "tmpf\${APP_NAME}-win32-x64" -mem=AES256/)
            bat(/move "win-tmpf\${APP_NAME}-win32-x64-SQUIRREL\tnt_modder-*-setup.exe" win-releases\tnt_modder-x64-setup.exe/)
            bat(/move "win-tmpf\${APP_NAME}-win32-x64-SQUIRREL\tnt_modder-*-*.nupkg" win-releases\tnt_modder-x64-full.nupkg/)
            bat(/move "win-tmpf\${APP_NAME}-win32-ia32-SQUIRREL\tnt_modder-*-setup.exe" win-releases\tnt_modder-ia32-setup.exe/)
            bat(/move "win-tmpf\${APP_NAME}-win32-ia32-SQUIRREL\tnt_modder-*-*.nupkg" win-releases\tnt_modder-ia32-full.nupkg/)
            bat(/pscp -i \id_rsa.ppk -hostkey 36:f5:5e:f1:97:27:85:a2:d7:42:20:30:93:a6:6a:00 "win-releases\*" server@strodl.tk:\/var\/www\/html\/strodl\/tnt-modder\/ci\/${buildnum}/)
        }
    }
})