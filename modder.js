let fs = require('fs-extra'), // So we can rm -rf!
    path = require('path'), // For joining paths and getting the path seperator
    xml2js = require('xml2js'), // Used to merge two XML files
    tmp = require('tmp'), // Used to get the temp dir for extracting mods
    unzip = require('unzip'), // Used to unzip mods
    zip = new require('node-zip')(), // Used to make mods
    mkdirp = require('mkdirp'); // Used to create dirs recursively

module.exports.getMods = (modFolder, callback) => {
    fs.readdir(modFolder, (err, files) => {
        if (err) throw new Error('Line 11: ' + err);
        fileloop({
            i: 0,
            files,
            modFolder,
            mods: {}
        }, callback);
    });
}

function fileloop(dat, callback) {
    console.log(dat);
    let {i, files, modFolder, mods} = dat;
    let file = files[i];
    if (i >= files.length) {
        return callback(mods);
    }
    if (path.extname(file).toLowerCase() != '.tnt') { // If the file isn't a mod
        console.log('Skipped '+file);
        return fileloop({
            i: i + 1,
            files,
            modFolder,
            mods
        }, callback); // Then skip it!
    }
    file = path.join(modFolder, file);
    module.exports.getModInfo(file, (r) => {
        mods[file] = {r, file}
        fileloop({
            i: i + 1,
            files,
            modFolder,
            mods
        }, callback);
    });
}

module.exports.createManifest = (moddir, info, callback) => {
    let manifest = {
        Name: info.name,
        Author: info.author,
        Description: info.description,
        IsInstalled: false,
        ModCount: 0,
        Mods: []
    }
    walk(moddir, (e, files) => {
        if (e) throw new Error('Line 58: ' + e);
        files.forEach((file) => {
            file = file.replace(path.normalize(moddir) + path.sep, '');
            if (path.sep != '\\') {
                file = file.replace(path.sep, '\\'); // Mj's manager needs windows paths...
            }
            manifest.Mods.push({
                TargetFile: file,
                ModdedFile: path.basename(file),
                BackupFile: file,
                IsInstalled: false,
                ModType: info.type
            });
        });
        manifest.ModCount = files.length;
        callback(manifest);
    });
}

module.exports.createMod = function (moddir, info, callback) {
    module.exports.createManifest(moddir, info, function (manifest) {

        manifest.Mods.forEach(function (file) {
            fs.readFile(path.join(moddir, file.TargetFile), function (e, r) {
                if (e) throw new Error('Line 82: ' + e);
                zip.file(file.ModdedFile, r);
                console.log("Added: " + file.ModdedFile);
                let data = zip.generate({
                    base64: false,
                    compression: 'DEFLATE'
                });
                console.log(data);
                fs.writeFile(path.join(moddir, path.basename(moddir)) + '.tnt', function (e) {
                    if (e) throw new Error('Line 91: ' + e);
                    console.log("Sucessfully created mod at: " + path.join(moddir, path.basename(moddir)) + ".tnt");
                    callback(data);
                }, 'binary');
            });
        });
    });
}

// getModInfo(path to mod, callback(mod info));
module.exports.getModInfo = function (mod, callback) {
    fs.readFile(mod, function (e, r) {
        if (e) throw new Error('Line 103: ' + e);
        zip = new require('node-zip')(r,
            {
                base64: false,
                checkCRC32: true
            });

        callback(zip.files['package.json']._data);
    });
}

// Thanks StackOverflow! http://stackoverflow.com/a/5827895
// Lists directories and their subdirectories to find all files in a folder. walk(directory, callback)
var walk = function (dir, done) {
    var results = [];
    fs.readdir(dir, function (err, list) {
        if (err) return done(err);
        var pending = list.length;
        if (!pending) return done(null, results);
        list.forEach(function (file) {
            file = path.resolve(dir, file);
            fs.stat(file, function (err, stat) {
                if (err) return done(err);
                if (stat && stat.isDirectory()) {
                    walk(file, function (err, res) {
                        if (err) return done(err);
                        results = results.concat(res);
                        if (!--pending) done(null, results);
                    });
                } else {
                    results.push(file);
                    if (!--pending) done(null, results);
                }
            });
        });
    });
};

// patchGame(Array with a path to .tnt files, Path to game's install directory)
module.exports.patchGame = function (mods, gameDir, mycallback) {
    process.chdir(gameDir);
    mods = mods.reverse(); // Flip the array so that the higher up ones get run last and therefore overwrite
    console.log(mods);
    // modManis = [];
    let toBeApplied = {},
        backups = [],
        tmps = [],
        modDat = [];
    patchloop(-1, {
        toBeApplied,
        backups,
        tmps,
        modDat,
        mods,
        gameDir,
        mycallback
    }, mycallback);
}

function patchloop(n, datas) {
    let {toBeApplied, backups, tmps, modDat, mods, gameDir, mycallback} = datas;

    if (n >= mods.length) {
        console.log('Ending patchloop');
        return false;
    }
    tmp.dir({mode: '0777', prefix: 'tnt-modder-'}, (err, outdir, cleanup) => {
        n++;
        if (!mods[n]) {
            console.log('Ending patchloop #2');
            return false;
        }
        modDat.push({
            tmp: outdir,
            file: mods[n],
            index: n,
            clean: cleanup
        });
        let modfile = mods[n];
        console.log(n, mods, mods[n], 'patch1');
        tmps.push(outdir);
        console.log("Now applying mod: " + modDat[n].file + " In tempdir: " + modDat[n].tmp + "!");
        fs.createReadStream(modDat[n].file).pipe(unzip.Extract({path: outdir})).once('close', function () {
            // n = modDat[n].index;
            outdir = modDat[n].tmp;
            cleanup = modDat[n].clean;
            modfile = modDat[n].file;
            console.log("Unzipped " + modfile + " to " + outdir);
            fs.readFile(path.join(outdir, 'package.json'), function (e, r) {
                if (e) throw new Error('Line 177: ' + e);
                console.log("Read package.json for " + modfile + "!" + " In dir: " + outdir);
                // modManis.push(JSON.parse(r));
                let data = JSON.parse(r);
                fs.unlink(path.join(outdir, 'package.json'), (e) => {
                    if (e) throw new Error('Line 182: ' + e);
                    console.log('deleted ' + path.join(outdir, 'package.json'));
                });
                let truemodcounter = 0;
                data.Mods = data.Mods.filter(Boolean); // Ignore falsy values. Mj's manager likes to put random nulls in...
                console.log('Adding ' + data.Mods.length + ' files!');
                let modn = -1;
                let mydata = {}
                console.log('Patch loop #2 started');
                patchloop2(modn, {data, toBeApplied, backups, tmps, modDat, mods, gameDir, mycallback, mydata, outdir, n, truemodcounter});
            });
        });
    });


}

let patchloop2 = (modn, datas) => {
    let {data, toBeApplied, backups, tmps, modDat, mods, gameDir, mycallback, mydata, outdir, n, truemodcounter} = datas;
    modn++;
    let mod = data.Mods[modn];
    if (!mod) {
        // clearInterval(patchloop); // No reason to let it run endlessly...
        console.log('Returning because mod is unset');
        mycallback();
        return false;
    }
    console.log("File: " + mod.TargetFile + " #" + modn + " looper");
    // Replace \s with / because we need to support more than windows
    mod.TargetFile = mod.TargetFile.replace(/\\/gi, path.sep);
    mod.BackupFile = mod.BackupFile.replace(/\\/gi, path.sep);
    if (modn == 0) truemodcounter = 0;
    if (!backups[mod.BackupFile]) backups.push(mod.BackupFile);
    if (!backups[mod.TargetFile]) backups.push(mod.TargetFile); // Copy over the target just in case!
    if (path.extname(mod.ModdedFile) == '.xml' && !mod.Overwrite) {
        fs.readFile(path.join(outdir, mod.ModdedFile), function (e, r) {
            if (e) throw new Error('Line 206: ' + e);
            xml2js.parseString(r, function (e, jsonofmod) {
                if (e) throw new Error('Error parsing modded XML file: ' + mod.ModdedFile + '! ' + e);
                console.log("Parsed " + mod.ModdedFile + " #" + modn);
                toBeApplied[mod.TargetFile] = {
                    data: merge(toBeApplied[mod.ModdedFile], jsonofmod),
                    modfolder: outdir
                };
                fs.unlink(path.join(outdir, mod.ModdedFile), (err) => {
                    if (err) throw err;
                    console.log('Deleted ' + mod.ModdedFile + '!');
                    patchloop2(modn, {data, toBeApplied, backups, tmps, modDat, mods, gameDir, mycallback, mydata, outdir, n, truemodcounter});
                });
                mydata[mod.ModdedFile] = true;
                truemodcounter++;
            });
        });
    } else if (!mydata[mod.ModdedFile]) {
        console.log(mydata[mod.ModdedFiles], 'FACKIN');
        toBeApplied[mod.TargetFile] = {
            data: mod.ModdedFile,
            modfolder: outdir
        };
        console.log("Added file: " + mod.TargetFile + " #" + modn);
        truemodcounter++;
        patchloop2(modn, {data, toBeApplied, backups, tmps, modDat, mods, gameDir, mycallback, mydata, outdir, n, truemodcounter});
    }
    if (truemodcounter + 1 >= data.Mods.length) patchloop(n, {
        toBeApplied,
        backups,
        tmps,
        modDat,
        mods,
        gameDir,
        mycallback,
        mydata
    });
    // Nasty hack
    if (modn + 1 >= data.Mods.length && n + 1 >= mods.length) {
        beginPatching({
            toBeApplied,
            backups,
            tmps,
            modDat,
            mods,
            gameDir,
            mycallback,
            n,
            truemodcounter,
            data,
            outdir
        });
    }
}

let beginPatching = function (datas) {
    let {toBeApplied, backups, tmps, modDat, mods, gameDir, mycallback, n, truemodcounter, data, outdir, completecallback, mydata} = datas;
    if (n + 1 == mods.length && truemodcounter == data.Mods.length) {
        // actualpatch({
        //     toBeApplied,
        //     backups,
        //     tmps,
        //     modDat,
        //     mods,
        //     gameDir,
        //     mycallback
        // });
        console.log("Done adding files...");
        mods = mods.reverse(); // Switch it back
        // modManis = modManis.reverse(); // Flip so our indexes match up

        // Backup the files! :D
        backups.forEach(function (file, n) {
            console.log(file, n, 'Backup');
            if (path.extname(file)) {
                console.log('Make dir ' + path.join(gameDir, 'backups', path.dirname(file)));
                mkdirp(path.join(gameDir, 'backups', path.dirname(file)));
            } else {
                console.log('Make dir: ' + path.join(gameDir, 'backups', file));
                mkdirp(path.join(gameDir, 'backups', file));
            }

            if (!fs.existsSync(gameDir, file) && path.extname(file)) {
                fs.writeFile(path.join(gameDir, file), '');
            }
            if (!fs.existsSync(path.join(gameDir, 'backups', file)) && path.extname(file)) {
                console.log('Backed up ' + file);
                fs.createReadStream(path.join(gameDir, file)).pipe(fs.createWriteStream(path.join(gameDir, 'backups', file)));
            }

            if (n + 1 == backups.length) {
                console.log("Starting to patch game!");
                // Start reading the vanilla file
                let i = 0;
                ReadThemMods({
                    toBeApplied,
                    gameDir,
                    mycallback,
                    data,
                    i,
                    outdir,
                    backups,
                    tmps,
                    modDat,
                    n,
                    mods,
                    completecallback,
                    mydata
                });
            }
        });
    }
}

function ReadThemMods(datas) {
    let {toBeApplied, gameDir, mycallback, data, i, outdir, backups, modDat, n, tmps, mods, completecallback, mydata} = datas;
    i += 1;
    let file = Object.keys(toBeApplied)[i - 1];
    if (i >= Object.keys(toBeApplied).length) {
        mycallback();
        return;
    }
    console.log(i, file, 'In patch');
    // if (!file) return false;
    if (i == Object.keys(toBeApplied).length) {
        if (fs.existsSync(outdir)) {
            fs.remove(outdir, (e) => {
                if (e) throw new Error('Line 279: ' + e);
                console.log('Removed ' + outdir + '!');
                patchloop(n, {toBeApplied, backups, tmps, modDat, mods, gameDir, mycallback}, completecallback);
            });
        } else {
            console.log('Removed ' + outdir + '!');
            patchloop(n, {toBeApplied, backups, tmps, modDat, mods, gameDir, mycallback}, completecallback);
        }
    }
    if (path.extname(file).toLowerCase() == '.xml') {
        mydata[file] = true;
        fs.readFile(path.join(gameDir, 'backups', file), function (e, r) {
            if (e) throw new Error('Line 291: ' + e);
            xml2js.parseString(r, function (e, vanillajson) {
                if (e) throw new Error('Line 293: ' + e);
                data = merge(vanillajson, toBeApplied[file].data);
                let xml = new xml2js.Builder().buildObject(data);
                fs.writeFile(path.join(gameDir, file), xml, (e) => {
                    if (e) throw new Error('Line 297: ' + e);
                    console.log("Patched file: " + file + "!");
                    ReadThemMods({toBeApplied, gameDir, mycallback, data, i, outdir, backups, modDat, n, tmps, mods, completecallback, mydata});
                });
            });
        });
    } else if (!mydata[file]) {
        console.log('Replacing ' + file + " #" + i + "!");
        fs.createReadStream(path.join(toBeApplied[file].modfolder, toBeApplied[file].data)).pipe(fs.createWriteStream(path.join(gameDir, file)).once('close', () => {
            console.log("Replaced file: " + file + " #" + i + "!");
            fs.unlink(path.join(toBeApplied[file].modfolder, toBeApplied[file].data), function (e) {
                if (e) throw new Error('Line 307: ' + e);
                console.log('Deleted ' + path.join(toBeApplied[file].modfolder, toBeApplied[file].data) + "!");
                ReadThemMods({toBeApplied, gameDir, mycallback, data, i, outdir, backups, modDat, n, tmps, mods, completecallback, mydata});
            });
        }));
    }
}

// Taken from this Gist: https://gist.github.com/svlasov-gists/2383751
function merge(target, source) {

    /* Merges two (or more) objects,
       giving the last one precedence */

    if (typeof target !== 'object' || !target) {
        target = {}
    }
    if (Object.prototype.toString.call(source) === '[object Array]') {
        return source; // Otherwise, the JSON would make a new key with the index
    }

    for (var property in source) {

        if (source.hasOwnProperty(property)) {

            var sourceProperty = source[property];
            if (typeof sourceProperty === 'object') {
                target[property] = merge(target[property], sourceProperty);
                continue;
            }
            target[property] = sourceProperty;

        }

    }

    for (var a = 2, l = arguments.length; a < l; a++) {
        merge(target, arguments[a]);
    }

    return target;
}

// For testing...
// module.exports.patchGame(
//     ["/Users/matthew/Desktop/tnt-modder/Mods/Test Mod.TNT",
//     "/Users/matthew/Desktop/tnt-modder/Mods/Goddamn Snakes.TNT"],
//     "/Users/matthew/Library/Application Support/Steam/steamapps/common/ToothAndTail/ToothAndTail.app/Contents/MacOS",
//     function() {
//         console.log('Game patched! \o/');
//     });
