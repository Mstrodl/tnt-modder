#!/bin/bash

echo "This is intended to be run on a Mac!"

APP_DIR=`pwd`
APP_NAME="Tooth and Tail Mod Manager"

WINMETA='--win32metadata.CompanyName=\"heatingdevice\" --win32metadata.FileDescription=\"Tooth and Tail Mod Manager\" --win32metadata.ProductName=\"Tooth and Tail Mod Manager\" --win32metadata.InternalName=$APP_NAME'

rm -rf tmp releases
mkdir releases tmp
cd tmp

echo "Generating packages..."

electron-packager $APP_DIR "$APP_NAME" --platform=darwin --arch=x64 --icon=../logo --protocol-name=\"Tooth And Tail Mod Manager\" --protocol=\"tnt-modder\"
electron-packager $APP_DIR "$APP_NAME" --platform=linux --arch=x64 --icon=../logo
electron-packager $APP_DIR "$APP_NAME" --platform=win32 --arch=x64 --icon=../logo $WINMETA
electron-packager $APP_DIR "$APP_NAME" --platform=linux --arch=ia32 --icon=../logo
electron-packager $APP_DIR "$APP_NAME" --platform=win32 --arch=ia32 --icon=../logo $WINMETA

echo "Packaged $APP_NAME! Compressing..."

echo "Doing Mac"

git clone https://github.com/andreyvit/yoursway-create-dmg.git create-dmg

./create-dmg/create-dmg \
--volname "$APP_NAME" \
--volicon "../logo.icns" \
--background "../dmg-background.png" \
--window-pos 200 120 \
--window-size 778 400 \
--icon-size 100 \
--icon "$APP_NAME.app" 200 190 \
--hide-extension "$APP_NAME.app" \
--app-drop-link 600 185 \
"$APP_NAME-darwin-x64.dmg" \
"$APP_NAME-darwin-x64/"

rm -rf "$APP_NAME-darwin-x64/" create-dmg/
mv "$APP_NAME-darwin-x64.dmg" ../releases 

# mkdir "../releases/$APP_NAME-win32-ia32-SQUIRREL" "../releases/$APP_NAME-win32-x64-SQUIRREL"

# electron-installer-squirrel-windows "$APP_NAME-win32-ia32" --out="../releases/$APP_NAME-win32-ia32-SQUIRREL" --debug
# electron-installer-squirrel-windows "$APP_NAME-win32-x64" --out="../releases/$APP_NAME-win32-x75-SQUIRREL" --debug

for f in *; do zip -r "../releases/$f.zip" "$f"; done

echo "Finished creating release archives for $APP_NAME"

echo "Deleting build dir..."

cd $APP_DIR
#rm -rf tmp

echo "Finished creating builds! You may now upload!"
