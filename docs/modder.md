#Modder

##modder.getMods
Used to loop over a folder and read all the mods in it.
```
modder.getMods(modFolder, callback);
```
modFolder: The folder to loop over
Callback:
    * mods: Object indexed by mod filenames containing the mod's `package.json` in the `r` key and the file's path in the `file` key

#modder.createManifest
The first step of creating a mod is contained here. This creates the mod's JSON manifest.
```
modder.createManifest(moddir, info, callback);
```
moddir: Absolute path to the root directory of the mod you want to generate a manifest for.
info: A JSON object containing mod name in the `name` key, the mod author's name in the `author` key, and the mod's description in `description` key.
callback:
    * Manifest: A JSON object which is the mod's manifest. **NOTE** You need to `JSON.stringify()` this before you save it since it's an object, not a string.

#modder.createMod
A wrapper for [modder.createManifest()](#modder.createManifest) which actually saves the manifest and compresses the mod folder to give a .TNT archive.
```
modder.createMod(moddir, info, callback);
```
moddir: Absolute path to the root directory of the mod you want to generate a manifest for.
info: A JSON object containing mod name in the `name` key, the mod author's name in the `author` key, and the mod's description in `description` key.
callback:
    * data: Binary data of the TNT archive. This is probably not necessary.

#modder.getModInfo
Used to read `package.json` from a give TNT archive.
```
modder.getModInfo(mod, callback);
```
mod: Path to the mod to read the `package.json` from.
callback:
    * data: Raw data from `package.json` in a string. This still needs to be `JSON.stringify()`'d to read data from it.

#modder.patchGame
Used to patch the game from an array of TNT archives.
```
modder.patchGame(mods, gameDir, callback);
```
mods: An array with paths to TNT archives to patch the game with.
gameDir: Path to the game to be patched
callback: No arguments.
