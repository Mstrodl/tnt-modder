console.time('init');
console.time('modules');
let {
    app,
    crashReporter,
    autoUpdater,
    BrowserWindow,
    dialog,
    ipcMain
} = require('electron'),
    path = require('path'),
    mkdirp = require('mkdirp'),
    url = require('url'),
    // {exec} = require('child_process'),
    open = require('open'),
    http = require('http');

const UpdateURLBase = 'http://squirrel.strodl.tk/update/';
let UpdateURL = '';
let EnableUpdater = false;

const ChildProcess = require('child_process');

const appFolder = path.resolve(process.execPath, '..');
const updateDotExe = path.resolve(path.join(appFolder, 'squirrel.exe'));
const exeName = path.basename(process.execPath);

console.timeEnd('modules');

// this should be placed at top of main.js to handle setup events quickly
if (handleSquirrelEvent()) {
    // squirrel event handled and app will exit in 1000ms, so don't do anything else
    return;
}

function handleSquirrelEvent() {
    if (process.argv.length === 1) {
        return false;
    }

    const spawn = function (command, args) {
        let spawnedProcess;

        try {
            spawnedProcess = ChildProcess.spawn(command, args, {detached: true});
        } catch (err) {
            console.log(err);
        }

        return spawnedProcess;
    };

    const spawnUpdate = function (args) {
        return spawn(updateDotExe, args);
    };

    const squirrelEvent = process.argv[1];
    switch (squirrelEvent) {
        case '--squirrel-install':
        case '--squirrel-updated':
            // Optionally do things such as:
            // - Add your .exe to the PATH
            // - Write to the registry for things like file associations and
            //   explorer context menus

            app.setAsDefaultProtocolClient('tnt-modder');

            // Install desktop and start menu shortcuts
            spawnUpdate(['--createShortcut', exeName]);

            setTimeout(app.quit, 1000);
            return true;

        case '--squirrel-uninstall':
            // Undo anything you did in the --squirrel-install and
            // --squirrel-updated handlers

            // Remove desktop and start menu shortcuts
            spawnUpdate(['--removeShortcut', exeName]);

            setTimeout(app.quit, 1000);
            return true;

        case '--squirrel-obsolete':
            // This is called on the outgoing version of your app before
            // we update to the new version - it's the opposite of
            // --squirrel-updated

            app.quit();
            return true;
        default: {
            break;
        }
    }
}

autoUpdater.on('error', (error) => {
    // We could throw an error but we want the app to continue to work despite the error
    console.log(error);
    // Tell the browser we had an error
    win.webContents.send('error', error);
});

autoUpdater.on('checking-for-update', () => {
    console.log('Checking for updates...');
    win.webContents.send('update-check');
})

autoUpdater.on('update-available', () => {
    console.log('Found an update!');
    win.webContents.send('update-found');
});

autoUpdater.on('update-not-available', () => {
    console.log('No updates found!');
    win.webContents.send('no-update-found');
});

autoUpdater.on('update-downloaded', (event, releaseNotes, releaseName) => {
    // , releaseData, updateURL
    console.log('Downloaded an update!');
    win.webContents.send('update-downloaded', {
        event,
        releaseNotes,
        releaseName,
        // releaseData,
        // updateURL,
        os: process.platform
    });
});

ipcMain.on('install-update', () => {
    console.log('Installing update!');
    autoUpdater.quitAndInstall();
});

console.time('crashReporter');
crashReporter.start({
    productName: 'Tooth and Tail Mod Manager',
    companyName: 'heatingdevice',
    submitURL: 'http://strodl.tk/breakpad/post',
    uploadToServer: true
});
console.timeEnd('crashReporter');

app.on('open-url', function (a, full) {
    let uri = full.replace('tnt-modder://', '');

    if (win) { // win is undefined if the app is unready...
        win.webContents.send('downloading', uri);
    }

    http.get(uri, (resp) => {
        resp.pipe(fs.createWriteStream(path.join(configFolder, 'mods', path.basename(uri))).once('finish', () => {
            readModJSON((data) => {
                // The easiest way to refresh mods is by having them sent again
                win.webContents.send('sendMods', JSON.stringify({
                    mods: data.mods,
                    enabled: data.enabled,
                    slideshow: data.slideshow,
                    tntfolder: data.tntfolder
                }));
            });
        }));
    });
});

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let win;

function createWindow() {
    let fail = app.setAsDefaultProtocolClient('tnt-modder');
    if (!fail) {
        console.warn('Failed to register URI: ' + fail);
    }

    // Create the browser window.
    let pref = {
        width: 1200,
        height: 700,
        minHeight: 700,
        minWidth: 1090,
        backgroundColor: '#202020',
        show: false
    }

    win = new BrowserWindow(pref);

    // and load the index.html of the app.
    console.time('load');
    console.time('ready');
    win.loadURL(url.format({
        pathname: path.join(__dirname, 'index.html'),
        protocol: 'file:',
        slashes: true
    }));

    win.once('ready-to-show', () => {
        // console.timeEnd('ready');
        // win.show();
    });

    // Emitted when the window is closed.
    win.on('closed', () => {
        // Dereference the window object, usually you would store windows
        // in an array if your app supports multi windows, this is the time
        // when you should delete the corresponding element.
        win = null;
    });
}

function selectTnT(callback) {
    let folder = dialog.showOpenDialog(BrowserWindow, {
        title: 'Select the directory where TnT is installed',
        properties: ['showHiddenFiles', 'openDirectory']
    });

    if (!folder) {
        throw new Error('No folder chosen for TnT!');
    }

    return callback(path.dirname(folder[0]));
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow);

// Quit when all windows are closed.
app.on('window-all-closed', () => {
    app.quit(); // Close the app when the all windows are closed. This may be skipped on MacOS because that's the normal functionality
})

app.on('activate', () => {
    // On macOS it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (win === null) {
        createWindow();
    }
})

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.

let modder = require('./modder.js'), // Core functions so others can make use of this too! :)
    fs = require('fs'), // For reading/writing user preferences
    // mods = {}, // Object containing all the mods
    enabled = {}, // Object containing all enabled mods
    disabled = {}, // Object containing all disabled mods
    configFolderName = '.tntmodder',
    configFileName = 'ModderData.json',
    tntfolder = '/',
    configFolder = path.join(__dirname, configFolderName);

// Detect the platform and place the config folder accordingly
switch (process.platform) {
    case 'darwin': {
        configFolder = path.join(process.env.HOME, 'Library', 'Application Support', configFolderName);
        tntfolder = path.join(process.env.HOME, 'Library', 'Application Support', 'Steam', 'steamapps', 'common', 'ToothAndTail', 'ToothAndTail.app', 'Contents');
        break;
    }
    case 'linux': {
        configFolder = path.join(process.env.HOME, configFolderName);
        tntfolder = path.join(process.env.HOME, '.steam', 'steamapps', 'common', 'ToothAndTail');
        break;
    }
    case 'win32': {
        configFolder = path.join(process.env.APPDATA, configFolderName);
        EnableUpdater = true;
        let progfolder = '/';

        switch (process.arch) {
            case 'x64': {
                progfolder = path.join('/', 'Program Files (x86)');
                UpdateURL = UpdateURLBase + 'win64';
                break;
            }
            case 'ia32': {
                progfolder = path.join('/', 'Program Files');
                UpdateURL = UpdateURLBase + 'win32';
                break;
            }
            default: {
                progfolder = path.join('/', 'Program Files');
                EnableUpdater = false;
                break;
            }
        }
        tntfolder = path.join(progfolder, 'Steam', 'ToothAndTail', 'steamapps', 'common');

        break;
    }
    default: {
        configFolder = path.join(__dirname, configFolderName);
        console.warn('WARNING: This is an unknown operating system! As a result, I don\'t know where to put my data! So I put it in: ' + configFolder + '!');
        break;
    }
}

// Create the folder if it doesn't already exist!
mkdirp(path.join(configFolder, 'mods'));
mkdirp(path.join(configFolder, 'themes'));

if (!fs.existsSync(path.join(configFolder, 'mods'))) fs.mkdirSync(path.join(configFolder, 'mods')); // I know, yucky synchronous functions, but this should be blocking because the folders need to exist before we can do anything with them :)
if (!fs.existsSync(path.join(configFolder, configFileName))) {
    fs.writeFile(path.join(configFolder, configFileName), JSON.stringify({
        enabled: {},
        slideshow: false
    }), function (e) {
        if (e) throw new Error(e);
    });
}

function readModJSON(callback) { // Reads the mod data and user preferences into the objects
    console.time('readjson');
    fs.readFile(path.join(configFolder, configFileName), 'utf-8', function (err, contents) {
        // console.log(contents);
        if (err) throw new Error(err);
        contents = JSON.parse(contents);

        // Clean up any non-existent files in the mod list...
        Object.keys(contents.enabled).forEach((val) => {
            if (!fs.existsSync(val)) delete contents.mods[val];
        });

        // enabled = contents.enabled || {};
        // tntfolder = contents.tntfolder || null;

        modder.getMods(path.join(configFolder, 'mods'), function (modData) {
            // mods = modData;
            let n = 0;
            for (var mod in modData) {
                if (modData.hasOwnProperty(mod)) {
                    n++;
                    console.log('Noice', mod);
                    if (contents.enabled[mod]) {
                        contents.enabled[mod] = true; // Add it to disabled if it isn't enabled
                        console.log('Defaulting ' + mod + ' to enabled!');
                    } else {
                        contents.enabled[mod] = false;
                        console.log('Setting ' + mod + ' to disabled');
                    }
                    if (n >= Object.keys(modData).length) {
                        console.log('End!');
                        console.timeEnd('init');
                        console.timeEnd('readjson');
                        return callback({
                            enabled: contents.enabled,
                            tntfolder: contents.tntfolder,
                            mods: modData,
                            slideshow: contents.slideshow
                        });
                    }
                }
            }
        });
    });
}

// function writeModJSON(callback) {
//     fs.writeFile(path.join(configFolder, configFileName), JSON.stringify({
//         tntfolder,
//         enabled,
//         disabled
//     }), (err) => {
//         callback(err);
//     });
// }

function enableMod(mod) {
    disabled.slice(mod.id, 1); // Remove from the disabled array
    enabled[mod.id] = mod.id; // Add it to the enabled array
    modder.enableMod(path.join(configFolder, 'mods', mod.id + '.hdm'));
}

function disableMod(mod) {
    enabled.slice(mod.id, 1);
    disabled[mod.id] = mod.id;
    modder.disableMod(path.join(configFolder, 'mods', mod.id + '.hdm'));
}

/*
readModJSON((data) => {
    if (!data.tntfolder || !fs.existsSync(tntfolder)) {
        selectTnT((folder) => {
            data.tntfolder = folder;
            console.log(folder, 'PLS');
            let str = JSON.stringify({
                tntfolder: data.tntfolder,
                enabled: data.enabled
            });
            let confpath = path.join(configFolder, configFileName);
            fs.writeFile(confpath, str, (err) => {
                if (err) throw new Error(err);
                console.log('Saved!');
            });
        });
    }
});
*/

ipcMain.on('SelectTnT', (event) => {
    selectTnT((folder) => {
        tntfolder = folder;
        event.sender.send('tntfolder', folder);
    });
});

ipcMain.on('bug', () => {
    open('https://gitlab.com/Mstrodl/tnt-modder/issues/new');
});

ipcMain.on('getMods', (event) => {
    if(EnableUpdater) {
        autoUpdater.setFeedURL(UpdateURL);
        autoUpdater.checkForUpdates();
    }
    console.timeEnd('load');
    readModJSON((data) => {
        if (!data.tntfolder || !fs.existsSync(tntfolder)) {
            selectTnT((folder) => {
                data.tntfolder = folder;
                event.sender.send('tntfolder', folder);
                let str = JSON.stringify({
                    enabled: data.enabled,
                    tntfolder: data.tntfolder,
                    slideshow: data.slideshow
                });
                let confpath = path.join(configFolder, configFileName);
                event.sender.send('tntfolder', folder);
                fs.writeFile(confpath, str, (err) => {
                    if (err) throw new Error(err);
                    console.log('Saved! Enabled:', JSON.parse(str).enabled);
                });
            });
        }
        event.sender.send('sendMods', JSON.stringify({
            mods: data.mods,
            enabled: data.enabled,
            slideshow: data.slideshow,
            tntfolder: data.tntfolder
        }));
        theme(event);
        console.log(disabled, enabled);
        console.timeEnd('ready');
        win.show();
    });
});

ipcMain.on('disable', (event, arg) => {
    disableMod(arg);
});

ipcMain.on('enable', (event, arg) => {
    enableMod(arg);
});

ipcMain.on('patch', (event, arg) => {
    console.log(arg.m, 'OOI MATE');
    modder.patchGame(arg.m, arg.tntfolder, () => {
        event.sender.send('patchdone');
    });
});

ipcMain.on('save', (event, arg) => {
    // save
    let args = JSON.parse(arg);
    let str = JSON.stringify({
        tntfolder,
        enabled: args.enabledmods,
        slideshow: args.slideshow
    });
    let confpath = path.join(configFolder, configFileName);
    fs.writeFile(confpath, str, (err) => {
        if (err) throw new Error(err);
        console.log('Saved!', str);
    });
});

// ipcMain.on('devtools', () => {
//     BrowserWindow.openDevTools();
// });

ipcMain.on('startgame', () => {
    // let executable = '';
    // if(process.platform == 'darwin') executable = 'ToothAndTail';
    // if(process.platform == 'win32') executable = 'ToothAndTail.exe';
    // if(process.platform == 'linux') executable = 'ToothAndTail';
    // exec(path.join(tntfolder, executable));
    open('steam://run/286000');
});

// Theme junk goes here... Right now, we just require() all the JS and CSS in a folder in the renderer process

function theme(event) {
    let css = [];
    let js = [];

    console.log('Theming up a school!');

    fs.readdir(path.join(configFolder, 'themes'), (err, files) => {
        if (err) {
            throw new Error(err);
        }

        files.forEach((file) => {
            file = path.join(configFolder, 'themes', file);

            switch (path.extname(file).toLowerCase()) {
                case '.css': {
                    css.push(file);
                    break;
                }
                case '.js': {
                    js.push(file);
                    break;
                }
                default: {
                    // Do nothing lol
                    break;
                }
            }
            event.sender.send('theme', {
                css,
                js
            });
        });
    });
}
