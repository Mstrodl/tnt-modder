^ Give a short but descriptive title above ^

A brief description of the bug:



Add screenshots!
* On Windows: press the Alt+Print Screen (May also be seen as Prnt Scrn) on your keyboard. This will copy a screenshot of the currently focused window. Right click in this area and paste the image to attach it to your issue request.
* On Mac: Press Command-Shift-4 and release. Then press the space bar and click the mouse on the window to take a screenshot of. A new image file will be added to your desktop. Drag the image into this area.
* On Linux: It depends on many factors. Look up your specific distro to find out how to take a screenshot.

Steps to reproduce the bug:

1. 
2. 
3. 
