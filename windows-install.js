var installer = require('electron-installer-windows');

let version = process.argv[2];
console.log('Version: '+version);

let config = {
    authors: ['heatingdevice'],
    exe: 'Tooth and Tail Mod Manager.exe',
    description: 'Tooth and Tail Mod Manager',
    version,
    name: 'tnt-modder',
    productName: 'Tooth and Tail Mod Manager',
    iconUrl: 'https://gitlab.com/Mstrodl/tnt-modder/raw/master/logo.ico',
    setupIcon: 'logo.ico',
    setupExe: 'Tooth and Tail Mod Manager-Setup.exe',
    setupMsi: 'Tooth and Tail Mod Manager-Setup.msi',
    noMsi: false
}

let config86 = config,
    config64 = config;

config86.src = 'tmpf/Tooth and Tail Mod Manager-win32-ia32';
config64.src= 'tmpf/Tooth and Tail Mod Manager-win32-x64';
config86.dest = 'tmpf/Tooth and Tail Mod Manager-win32-ia32-SQUIRREL';
config64.dest = 'tmpf/Tooth and Tail Mod Manager-win32-x64-SQUIRREL';
// config86.remoteReleases = 'http://squirrel.strodl.tk/update/win32/'+version+'/RELEASES';
// config64.remoteReleases = 'http://squirrel.strodl.tk/update/win64/'+version+'/RELEASES';

installer(config64, (err) => {
    if(err) throw new Error(err);
    console.log('Successfully created x64 installer!');
});

installer(config86, (err) => {
    if(err) throw new Error(err);
    console.log('Successfully created x86 installer!');
});
